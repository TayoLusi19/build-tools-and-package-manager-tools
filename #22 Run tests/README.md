# Exercise 22: Run Tests and Verify Fix

In this exercise, you'll address a failing test by correcting a mistaken data type and then run your project's test suite using Gradle to confirm the fix. This process emphasizes the importance of accurate data types in test assertions and demonstrates how to execute tests in isolation.

## Steps to Fix the Test and Run Tests

### Fixing the Test

1. **Locate the `AppTest.java` File:**

    Navigate to the `src/test/java` directory in your project, and open the `AppTest.java` file.

2. **Correct the Test:**

    Find the line of code causing the test to fail (line 22) due to a data type mismatch. Change the `"true"` string to a `true` boolean value:

    ```java
    boolean result = myApp.getCondition(true);
    ```

    This correction ensures that the test passes the correct data type to the method, matching its expected parameters.

### Running the Tests

3. **Execute Only the Tests:**

    With the test fixed, return to the root directory of your project where the `build.gradle` file is located. Run the following command in the terminal to execute the project's test suite:

    ```bash
    gradle test
    ```

    This command compiles and runs all tests in the project, reporting the results. Since the failing test has been corrected, you should now see that all tests pass.

## Best Practices

- **Isolate and Run Tests Regularly:** Regularly running your test suite with `gradle test` helps identify and fix errors early in the development process.
- **Understand the Data Types:** Ensure that the data types used in tests match those expected by the methods being tested. This prevents type mismatch errors that can cause tests to fail.
- **Review Test Output:** Carefully review the output of failed tests to understand the cause of the failure. Gradle provides detailed test reports that can help pinpoint issues.

By following these steps to fix the failing test and verifying the correction by running your test suite, you maintain the integrity and reliability of your project's codebase.

# Contributions
Contributions to this script are welcome. Please ensure to follow good coding practices and provide a detailed description of your changes or improvements.

# License
[LICENSE.md](https://gitlab.com/TayoLusi19/build-tools-and-package-manager-tools/-/blob/main/LICENSE.md?ref_type=heads)

# Author
Adetayo Michael Ibijemilusi

## Resume Bullet Point

- Corrected a data type in AppTest.java to resolve a test failure, reinforcing the accuracy of test assertions.
- Validated the fix by running gradle test, ensuring all tests pass and maintaining codebase reliability.