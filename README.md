# Build Tools And Package Manager Tools



## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/TayoLusi19/build-tools-and-package-manager-tools.git
git branch -M main
git push -uf origin main
```

# Resume Bullet Point


## Exercises 20
- Cloned build-tools-exercises and reset Git history to establish a personal project workspace.
- Pushed initial setup to a new GitLab repository, laying the groundwork for independent development.

## Exercises 21
- Addressed a compile error in tests to successfully execute gradle build, producing a JAR file artifact from a Java project.
- Applied best practices by separating test runs and advocating for CI integration, enhancing code reliability and build success.

## Exercises 22
- Corrected a data type in AppTest.java to resolve a test failure, reinforcing the accuracy of test assertions.
- Validated the fix by running gradle test, ensuring all tests pass and maintaining codebase reliability.

## Exercises 23
- Executed gradle clean to remove old build artifacts, ensuring a clean project state for accurate builds.
- Rebuilt the project with gradle build, confirming code and test updates are correctly incorporated into the new JAR file.

## Exercises 24
- Located and executed the build-tools-exercises-1.0-SNAPSHOT.jar file to start the application, ensuring operational functionality.
- Monitored startup logs for errors, affirming application readiness and stability for deployment or further testing.

## Exercises 25
- Enhanced Application.java to accept and log command-line parameters, enriching application functionality.
- Successfully rebuilt and executed the updated application with parameters, demonstrating dynamic input handling.