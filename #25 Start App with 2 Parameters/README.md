# Exercise 25: Start Application with Parameters

In this exercise, you will update your application to accept command-line parameters, enabling dynamic behavior based on user input. Follow the steps below to add parameter handling to your Java code, rebuild your application, and run it with two parameters.

## Steps to Add Parameters and Run the Application

### Update the Java Code

1. **Modify the `Application.java` File:**

    Open the `Application.java` file located in `src/main/java` (or the appropriate package directory) and navigate to line 16. Insert the following code snippet to add parameter handling:

    ```java
    Logger log = LoggerFactory.getLogger(Application.class);
    try {
        String one = args[0];
        String two = args[1];
        log.info("Application will start with the parameters {} and {}", one, two);
    } catch (Exception e) {
        log.info("No parameters provided");
    }
    ```

    This code tries to capture the first two parameters passed to the application, logs them, and handles the case where no parameters are provided by logging a different message.

### Rebuild the JAR File

2. **Rebuild the Project to Create an Updated JAR File:**

    With the code changes made, navigate to the root of your project where the `build.gradle` file is located and run:

    ```bash
    gradle build
    ```

    This command compiles your updated project, runs tests, and packages the compiled code into a JAR file.

### Run the Application with Parameters

3. **Execute the JAR File with Two Parameters:**

    Now, run the application with any two parameters of your choice. Replace `bootcamp-java-project-1.0-SNAPSHOT.jar` with the actual name of your JAR file:

    ```bash
    java -jar build/libs/bootcamp-java-project-1.0-SNAPSHOT.jar "myname" "mylastname"
    ```

    Ensure you are in the directory containing the JAR file, or adjust the path to where your JAR file is located.

## Best Practices

- **Validate Input Parameters:** Consider adding validation for input parameters to handle unexpected inputs gracefully.
- **Logging:** Utilize logging to capture important information and debug issues. The parameter logging introduced here can help understand how your application is being used.
- **Testing:** After adding new features or making changes, ensure your application is thoroughly tested, including the new parameter handling functionality.

By completing this exercise, you've enhanced your application to accept and log command-line parameters, providing a more dynamic and flexible user experience.

# Contributions
Contributions to this script are welcome. Please ensure to follow good coding practices and provide a detailed description of your changes or improvements.

# License
[LICENSE.md](https://gitlab.com/TayoLusi19/build-tools-and-package-manager-tools/-/blob/main/LICENSE.md?ref_type=heads)

# Author
Adetayo Michael Ibijemilusi

## Resume Bullet Point

- Enhanced Application.java to accept and log command-line parameters, enriching application functionality.
- Successfully rebuilt and executed the updated application with parameters, demonstrating dynamic input handling.