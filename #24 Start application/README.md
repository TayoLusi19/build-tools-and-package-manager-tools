# Exercise 24: Start Application from JAR File

After building your application into a JAR file, the next step is to run the application to verify its successful execution. This exercise walks you through the process of starting your Java application using the built JAR file.

## Steps to Start the Application

### Locate Your JAR File

1. **Find the JAR file in your project's build output directory:**

    After a successful build, the JAR file is located in `/build/libs` directory within your project. Ensure you identify the correct JAR file, which is typically named following the pattern `projectName-version.jar`.

### Start the Application

2. **Start the application using the Java command:**

    Replace `"app-1.0.jar"` with the actual name of your JAR file. In this case, we're using `bootcamp-java-project-1.0-SNAPSHOT.jar` as an example. Navigate to the `/build/libs` directory, then execute:

    ```bash
    java -jar build-tools-exercises-1.0-SNAPSHOT.jar
    ```

    This command starts the Java application packaged inside the JAR file. Monitor the output to verify that the application starts and runs without errors.

## Best Practices

- **Verify Application Requirements:** Ensure any required configurations or external resources the application might need at runtime are correctly set up before starting the application.
- **Monitor Application Logs:** Pay attention to the console output or application logs for any errors or warning messages that could indicate problems with the application's execution.
- **Use a Script for Starting the Application:** Consider creating a script file (e.g., `start.sh`) that contains the command to start the application. This can simplify the process of starting the app and ensure consistency if additional parameters or configurations are needed.

By following these steps, you can successfully start your Java application from a JAR file, verifying that it runs as expected and is ready for further testing or deployment.

# Contributions
Contributions to this script are welcome. Please ensure to follow good coding practices and provide a detailed description of your changes or improvements.

# License
[LICENSE.md](https://gitlab.com/TayoLusi19/build-tools-and-package-manager-tools/-/blob/main/LICENSE.md?ref_type=heads)

# Author
Adetayo Michael Ibijemilusi

## Resume Bullet Point

- Located and executed the build-tools-exercises-1.0-SNAPSHOT.jar file to start the application, ensuring operational functionality.
- Monitored startup logs for errors, affirming application readiness and stability for deployment or further testing.