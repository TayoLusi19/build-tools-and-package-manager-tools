# Exercise 21: Build Jar Artifact with Gradle

This exercise involves attempting to build a JAR file from a Java project using Gradle. The process demonstrates how to initiate a build and troubleshoot a common issue where the build fails due to a compile error in a test.

## Steps to Build Jar Artifact

### Attempting the Build

1. **Try to build the JAR file using Gradle:**

    In the terminal, navigate to the root directory of your project where the `build.gradle` file is located and run:

    ```bash
    gradle build
    ```

    This command compiles your project's source code, runs any tests, and attempts to package the compiled code into a JAR file.

### Handling the Compile Error in Tests

- If the build fails due to a compile error in a test, Gradle will output an error message indicating the source of the problem. The error message provides details about the test that failed to compile, including the file name and line number.

### Common Solutions

- **Review the Error Message:** Carefully read the error message to understand what caused the compile error. It could be due to a syntax error, missing dependencies, or incorrect use of an API.

- **Fix the Test:** Navigate to the specified file and line number in the error message. Correct the issue that caused the test to fail. This may involve fixing syntax errors, adding missing imports, or modifying the test logic.

- **Re-run the Build:** After fixing the error, save your changes and re-run `gradle build` to attempt to build the project again.

## Best Practices

- **Run Tests Separately:** Before attempting to build the entire project, consider running `gradle test` to specifically compile and execute tests. This can help identify issues early in the development cycle.

- **Use Continuous Integration:** Implement a continuous integration (CI) pipeline to automatically build your project and run tests on each commit. This helps catch errors quickly and improves code quality.

- **Write Reliable Tests:** Ensure your tests are reliable and testing the correct behavior. Flaky tests can hinder development and lead to false positives or negatives in your CI pipeline.

By following these steps and addressing the compile error in your tests, you can successfully build your project's JAR artifact, making it available for deployment or sharing with team members.

# Contributions
Contributions to this script are welcome. Please ensure to follow good coding practices and provide a detailed description of your changes or improvements.

# License
[LICENSE.md](https://gitlab.com/TayoLusi19/build-tools-and-package-manager-tools/-/blob/main/LICENSE.md?ref_type=heads)

# Author
Adetayo Michael Ibijemilusi

## Resume Bullet Point

- Addressed a compile error in tests to successfully execute gradle build, producing a JAR file artifact from a Java project.
- Applied best practices by separating test runs and advocating for CI integration, enhancing code reliability and build success.