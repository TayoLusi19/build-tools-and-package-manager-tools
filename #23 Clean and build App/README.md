# Exercise 23: Clean and Build App with Gradle

After fixing a test issue, it's a good practice to clean your project's build folder to remove any previous build artifacts. This ensures that your next build starts fresh and includes only the latest changes. This exercise guides you through cleaning your build folder and rebuilding your JAR file with Gradle.

## Steps to Clean and Build the Project

### Clean the Build Folder

1. **Clean your project's build folder:**

    This step removes the `build/` directory, which contains compiled classes, the JAR file, and other build artifacts. Cleaning helps prevent any potential conflicts or issues from previous builds.

    ```bash
    gradle clean
    ```

### Build the JAR File Again

2. **Rebuild your project to generate the JAR file:**

    After cleaning, use the `build` task to compile your project's source code, run tests, and package the compiled code into a JAR file.

    ```bash
    gradle build
    ```

    This command executes the full build lifecycle, ensuring that your project is compiled, tested, and packaged correctly.

## Best Practices

- **Regularly Clean Your Build:** Before starting a new build, especially after making significant changes or fixes, cleaning your build folder can help avoid issues related to stale artifacts.
- **Review Build Outputs:** After rebuilding your project, review the build outputs and test reports to ensure there are no unexpected errors or failures.
- **Use Continuous Integration:** Implementing a continuous integration (CI) pipeline can automate the clean and build processes, ensuring that your project is built and tested in a clean environment every time.

By following these steps to clean your project's build folder and rebuild your JAR file, you ensure that your build artifacts are up to date and reflect the most recent changes in your project.

# Contributions
Contributions to this script are welcome. Please ensure to follow good coding practices and provide a detailed description of your changes or improvements.

# License
[LICENSE.md](https://gitlab.com/TayoLusi19/build-tools-and-package-manager-tools/-/blob/main/LICENSE.md?ref_type=heads)

# Author
Adetayo Michael Ibijemilusi

## Resume Bullet Point

- Executed gradle clean to remove old build artifacts, ensuring a clean project state for accurate builds.
- Rebuilt the project with gradle build, confirming code and test updates are correctly incorporated into the new JAR file.