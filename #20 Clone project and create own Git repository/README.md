# Exercise 20: Clone Project and Create Own Git Repository

This initial setup exercise guides you through the process of cloning an existing project and setting up your own Git repository for the exercises. This ensures you have a clean slate for your work.

## Steps to Clone and Set Up Your Own Repository

### Clone the Project

1. **Clone the repository to your local machine:**

    ```bash
    git clone git@gitlab.com:twn-devops-bootcamp/latest/04-build-tools/build-tools-exercises.git
    ```

2. **Change into the project directory:**

    ```bash
    cd build-tools-exercises
    ```

### Remove Original Git History and Initialize New Repository

3. **Remove the existing `.git` directory to start fresh:**

    ```bash
    rm -rf .git
    ```

4. **Initialize a new Git repository:**

    ```bash
    git init
    ```

5. **Add all existing files to the repository and commit them:**

    ```bash
    git add .
    git commit -m "Initial commit"
    ```

### Create and Push to New GitLab Repository

6. **Create a new repository on GitLab.**

    - Log in to your GitLab account.
    - Navigate to the "Repositories" section and click "New project".
    - Follow the prompts to create a new repository.

7. **Link your local repository to your new GitLab repository:**

    Replace `{gitlab-user}` and `{gitlab-repo}` with your GitLab username and the new repository name, respectively.

    ```bash
    git remote add origin git@gitlab.com:{gitlab-user}/{gitlab-repo}.git
    ```

8. **Push your local repository to GitLab:**

    ```bash
    git push -u origin master
    ```

    This command pushes your commits to the master branch of your remote repository on GitLab, setting it as the upstream (default) for future pushes.

## Best Practices

- **Keep Your Repository Clean:** Starting with a fresh repository helps avoid confusion and ensures that only relevant files are included.
- **Commit Regularly:** Make it a habit to commit your changes frequently with descriptive messages. This practice helps track progress and makes it easier to revert changes if needed.
- **Secure Your Credentials:** When cloning repositories or setting up remote connections, ensure your Git credentials are secure, especially when using public or shared computers.

By following these steps, you're now ready to start working on the exercises with your own version of the project, hosted in your GitLab repository.

# Contributions
Contributions to this script are welcome. Please ensure to follow good coding practices and provide a detailed description of your changes or improvements.

# License
[LICENSE.md](https://gitlab.com/TayoLusi19/build-tools-and-package-manager-tools/-/blob/main/LICENSE.md?ref_type=heads)

# Author
Adetayo Michael Ibijemilusi

## Resume Bullet Point

- Cloned build-tools-exercises and reset Git history to establish a personal project workspace.
- Pushed initial setup to a new GitLab repository, laying the groundwork for independent development.